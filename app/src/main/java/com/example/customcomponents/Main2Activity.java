package com.example.customcomponents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.customcomponents.com.example.nestedscrollview.Model_Detail;
import com.example.customcomponents.com.example.nestedscrollview.Model_Header;
import com.example.customcomponents.com.example.nestedscrollview.RecyclerAdapter;
import com.example.customcomponents.com.example.nestedscrollview.Section;
import com.example.customcomponents.com.example.nestedscrollview.StickyItemDecoration;


import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    FrameLayout frameLayout;
    RecyclerView recyclerView;
    ArrayList<Section> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        frameLayout = findViewById(R.id.frame_layout_main2);
        recyclerView = findViewById(R.id.recycler_view_main2);
        //Toast.makeText(this, String.valueOf(getResources().getConfiguration().fontScale),Toast.LENGTH_SHORT).show();
        populateItem();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new RecyclerAdapter(data));
        recyclerView.addItemDecoration(new StickyItemDecoration(new StickyItemDecoration.SectionCallback() {
            @Override
            public String getHeaderName(int pos) {
                return data.get(pos).getHeaderName();
            }

            @Override
            public String getHeaderSize(int pos) {
                return ((Model_Header) data.get(pos)).getRoom_size();
            }

            @Override
            public String getHeaderType(int pos) {
                return ((Model_Header) data.get(pos)).getRoom_type();
            }

            @Override
            public boolean isHeader(int pos) {
                return data.get(pos).isHeader();
            }
        }));
    }

    private void populateItem() {
        data = new ArrayList<>();
        data.add(new Model_Header(1, "Superior Room", "1 Double Bed", "269 sqft"));
        data.add(new Model_Detail(1, "3073"));
        data.add(new Model_Header(2, "Superior Suite Room", "1 Double Bed", "301 sqft"));
        data.add(new Model_Detail(2, "3553"));
        data.add(new Model_Header(3, "Deluxe Room", "1 Twin Bed", "400 sqft"));
        data.add(new Model_Detail(3, "3733"));
        data.add(new Model_Header(4, "Deluxe Suite Room", "1 King Bed", "450 sqft"));
        data.add(new Model_Detail(4, "4000"));

        data.add(new Model_Header(5, "Superior Room", "1 Double Bed", "269 sqft"));
        data.add(new Model_Detail(5, "3073"));
        data.add(new Model_Header(6, "Superior Suite Room", "1 Double Bed", "301 sqft"));
        data.add(new Model_Detail(6, "3553"));
        data.add(new Model_Header(7, "Deluxe Room", "1 Twin Bed", "400 sqft"));
        data.add(new Model_Detail(7, "3733"));
        data.add(new Model_Header(8, "Deluxe Suite Room", "1 King Bed", "450 sqft"));
        data.add(new Model_Detail(8, "4000"));
    }
}
