package com.example.customcomponents.com.example.nestedscrollview;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.customcomponents.R;

public class StickyItemDecoration extends RecyclerView.ItemDecoration {

    private SectionCallback sectionCallback;

    public StickyItemDecoration(SectionCallback sectionCallback) {
        this.sectionCallback = sectionCallback;
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        /*
        if(header==null){
            header = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_header,parent,false);
            fixLayoutChanges(header,parent);
        }

        for(int i=0;i<parent.getChildCount();i++){
            View recyclerItem = parent.getChildAt(i);
            int position = parent.getChildAdapterPosition(recyclerItem);
            if(parent.getAdapter().getItemViewType(position)==0){
                c.save();
                c.translate(0,Math.max(recyclerItem.getBottom()-header.getHeight(),0));
                recyclerItem.draw(c);
                c.restore();
                BindText(position);
           }
        }
        */

        View childView = parent.getChildAt(0);
        int posChild = parent.getChildAdapterPosition(childView);
        Log.i("sahib", "posChild " + posChild);
        View currentHeader = getHeaderViewForItem(posChild, parent);
        fixLayoutChanges(currentHeader, parent);
        int contactPoint = currentHeader.getBottom();
        Log.i("sahib", "contactPoint " + contactPoint);
        View childInContact = getChildInContact(parent, contactPoint);

        if (parent.getAdapter().getItemViewType(parent.getChildAdapterPosition(childInContact)) == 0) {
            moveHeader(c, currentHeader, childInContact);
            return;
        }
        drawHeader(c, currentHeader);
    }

    private View getChildInContact(RecyclerView parent, int contactPoint) {
        View childInContact = null;
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child.getBottom() > contactPoint) {
                if (child.getTop() <= contactPoint) {
                    Log.i("sahib", "child in contact " + i);
                    // This child overlaps the contactPoint
                    childInContact = child;
                    break;
                }
            }
        }
        return childInContact;
    }


    private int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (sectionCallback.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    /*
     * Draws the Header at top of screen
     */
    private void drawHeader(Canvas c, View child) {
        c.save();
        c.translate(0, 0);
        child.draw(c);
        c.restore();
    }

    /*
     * Moves The Header for Push Effect
     */
    private void moveHeader(Canvas c, View currentHeader, View nextHeader) {
        c.save();
        c.translate(0, nextHeader.getTop() - currentHeader.getHeight());
        currentHeader.draw(c);
        c.restore();
    }

    /*
     * Gets Header for Current @Param itemPosition and returns it
     */
    private View getHeaderViewForItem(int itemPosition, RecyclerView parent) {
        View header = null;
        int headerPos = getHeaderPositionForItem(itemPosition);
        header = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_header, parent, false);
        BindText(header, headerPos);
        return header;
    }

    private void fixLayoutChanges(View v, ViewGroup parent) {
        if (v.getLayoutParams() == null) {
            v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        int viewWidth = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        int viewHeight = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

        int childWidth = ViewGroup.getChildMeasureSpec(viewWidth,
                parent.getPaddingLeft() + parent.getPaddingRight(), v.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(viewHeight,
                parent.getPaddingTop() + parent.getPaddingBottom(), v.getLayoutParams().height);

        v.measure(childWidth, childHeight);
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
    }

    /*
     * Binds Data To Header View
     */
    private void BindText(View header, int pos) {
        if (sectionCallback.isHeader(pos)) {
            TextView name = header.findViewById(R.id.tv_room_name_header);
            TextView size = header.findViewById(R.id.tv_room_size_header);
            TextView type = header.findViewById(R.id.tv_room_type_header);
            name.setText(sectionCallback.getHeaderName(pos));
            size.setText(sectionCallback.getHeaderSize(pos));
            type.setText(sectionCallback.getHeaderType(pos));
        }
    }

    /*
     * Interface To be implemented
     */
    public interface SectionCallback {
        String getHeaderName(int pos);

        String getHeaderSize(int pos);

        String getHeaderType(int pos);

        boolean isHeader(int pos);
    }

}