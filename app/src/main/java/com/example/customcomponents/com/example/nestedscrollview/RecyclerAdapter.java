package com.example.customcomponents.com.example.nestedscrollview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.customcomponents.R;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Section> data;
    private final int VIEWTYPE_HEADER = 0;
    private final int VIEWTYPE_DETAIL = 1;

    public RecyclerAdapter(ArrayList<Section> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int view_type) {
        View v;
        if (view_type == 1) {//DETAIL VIEWTYPE
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_list_item, viewGroup, false);
            return new DetailViewHolder(v);
        } else {//HEADER VIEWTYPE
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_header, viewGroup, false);
            return new HeaderViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, int pos) {
        if (data.get(pos).isHeader()) {
            Model_Header instance = (Model_Header) data.get(pos);
            ((HeaderViewHolder) myViewHolder).room_name.setText(instance.getRoom_name());
            ((HeaderViewHolder) myViewHolder).room_type.setText(instance.getRoom_type());
            ((HeaderViewHolder) myViewHolder).room_size.setText(instance.getRoom_size());
        } else {
            Model_Detail instance = (Model_Detail) data.get(pos);
            ((DetailViewHolder) myViewHolder).room_price.setText(instance.getRoom_price());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).isHeader()) return VIEWTYPE_HEADER;
        return VIEWTYPE_DETAIL;
    }

    /*
    View Holders For Header and Detail View
     */

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView room_name;
        TextView room_size;
        TextView room_type;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            room_name = itemView.findViewById(R.id.tv_room_name_header);
            room_size = itemView.findViewById(R.id.tv_room_size_header);
            room_type = itemView.findViewById(R.id.tv_room_type_header);
        }
    }

    public class DetailViewHolder extends RecyclerView.ViewHolder {

        TextView room_price;
        Button btn_room_book;

        public DetailViewHolder(@NonNull View itemView) {
            super(itemView);
            room_price = itemView.findViewById(R.id.tv_room_price);
            btn_room_book = itemView.findViewById(R.id.btn_room_book);
        }
    }
}
