package com.example.customcomponents.com.example.nestedscrollview;

public interface Section {
    Boolean isHeader();

    String getHeaderName();

    Integer getSectionPosition();
}
