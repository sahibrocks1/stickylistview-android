package com.example.customcomponents.com.example.nestedscrollview;

public class Model_Detail implements Section {

    String room_price;
    Integer section;

    public Model_Detail(Integer section, String room_price) {
        this.room_price = room_price;
        this.section = section;
    }

    @Override
    public Boolean isHeader() {
        return false;
    }

    @Override
    public String getHeaderName() {
        return "Section " + section.toString();
    }

    @Override
    public Integer getSectionPosition() {
        return section;
    }

    /*
    Getters And Setters
     */
    public String getRoom_price() {
        return room_price;
    }

    public void setRoom_price(String room_price) {
        this.room_price = room_price;
    }

    public Integer getSection() {
        return section;
    }

    public void setSection(Integer section) {
        this.section = section;
    }
}
