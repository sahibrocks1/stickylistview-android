package com.example.customcomponents.com.example.nestedscrollview;

public class Model_Header implements Section {

    private String room_name;
    private String room_type;
    private String room_size;
    private Integer section;

    public Model_Header(Integer section, String room_name, String room_type, String room_size) {
        this.section = section;
        this.room_name = room_name;
        this.room_type = room_type;
        this.room_size = room_size;
    }

    @Override
    public Boolean isHeader() {
        return true;
    }

    @Override
    public String getHeaderName() {
        return room_name;
    }

    @Override
    public Integer getSectionPosition() {
        return section;
    }

    /*
    Setters And Getters
     */
    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRoom_size() {
        return room_size;
    }

    public void setRoom_size(String room_size) {
        this.room_size = room_size;
    }

    public Integer getSection() {
        return section;
    }

    public void setSection(Integer section) {
        this.section = section;
    }
}
