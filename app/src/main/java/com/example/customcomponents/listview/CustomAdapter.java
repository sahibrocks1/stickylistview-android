package com.example.customcomponents.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.customcomponents.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Model_Room> data;

    public CustomAdapter(Context context, ArrayList<Model_Room> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.test_list_item, parent, false);
            BindView(position, view);
            return view;
        }
        BindView(position, view);
        return view;
    }

    private void BindView(int position, View view) {
        TextView room_type = view.findViewById(R.id.tv_room_type_header);
        TextView room_name = view.findViewById(R.id.tv_room_name_header);
        TextView room_size = view.findViewById(R.id.tv_room_size_header);
        room_type.setText(data.get(position).getRoom_type());
        room_name.setText(data.get(position).getRoom_size());
        room_size.setText(data.get(position).getRoom_area());
        TextView room_price = view.findViewById(R.id.tv_room_price);
        room_price.setText(data.get(position).getRoom_price());
    }

}
