package com.example.customcomponents.listview;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.customcomponents.R;
import com.example.customcomponents.listview.CustomAdapter;
import com.example.customcomponents.listview.Model_Room;

import java.util.ArrayList;

public class StickyHeader extends RelativeLayout implements AbsListView.OnScrollListener {

    private RelativeLayout header;
    private ListView listView;
    private ArrayList<Model_Room> data;
    private int prev_item_num = 0;
    private Boolean isShowingHeader = false;

    /**
     * Default Constructors
     *
     * @param context
     */
    public StickyHeader(Context context) {
        super(context);
    }

    public StickyHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StickyHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StickyHeader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void setData(ArrayList<Model_Room> data) {
        this.data = data;
    }

    public void init(ArrayList<Model_Room> data) {
        inflate(getContext(), R.layout.test_xml, this);
        listView = findViewById(R.id.listview_test);
        listView.setOnScrollListener(this);
        header = findViewById(R.id.header_test);
        setData(data);
        listView.setAdapter(new CustomAdapter(getContext(), data));
    }


    private void showStickyUI(int pos) {
        if (!isShowingHeader) {
            header.setVisibility(VISIBLE);
            isShowingHeader = true;
            changeUI();
        }
        bindHeader(pos);
    }

    private void changeUI() {
        LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if (isShowingHeader) {
            layoutParams.bottomMargin += header.getHeight();
            layoutParams.alignWithParent = true;
            ObjectAnimator animation = ObjectAnimator.ofFloat(listView, "y", header.getHeight());
            animation.setDuration(500);
            animation.start();
        } else {
            layoutParams.bottomMargin = 0;
            ObjectAnimator animation = ObjectAnimator.ofFloat(listView, "translationY", 0);
            animation.setDuration(500);
            animation.start();
        }
        listView.setLayoutParams(layoutParams);
    }

    private void bindHeader(int pos) {
        TextView room_type = header.findViewById(R.id.tv_room_type_header);
        TextView room_name = header.findViewById(R.id.tv_room_name_header);
        TextView room_size = header.findViewById(R.id.tv_room_size_header);
        room_type.setText(data.get(pos).getRoom_type());
        room_name.setText(data.get(pos).getRoom_size());
        room_size.setText(data.get(pos).getRoom_area());
    }

    private void hideStickyUI() {
        if (isShowingHeader) {
            header.setVisibility(View.INVISIBLE);
            isShowingHeader = false;
            changeUI();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        //Up Scroll
        if (firstVisibleItem > 0) {
            showStickyUI(firstVisibleItem - 1);
        } else if (prev_item_num > 0 && firstVisibleItem == 0) {
            hideStickyUI();
        }
        prev_item_num = firstVisibleItem;
    }
}
