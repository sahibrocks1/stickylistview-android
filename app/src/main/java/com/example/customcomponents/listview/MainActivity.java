package com.example.customcomponents.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.customcomponents.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    StickyHeader stickyHeader;
    ArrayList<Model_Room> model_rooms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        populateItem();
        stickyHeader = findViewById(R.id.listview_main);
        stickyHeader.init(model_rooms);
    }

    private void populateItem() {
        model_rooms = new ArrayList<>();
        model_rooms.add(new Model_Room("Superior Room", "269 sqft", "1 Double Bed", "3173"));
        model_rooms.add(new Model_Room("Superior Suite Room", "301 sqft", "1 Double Bed", "3553"));
        model_rooms.add(new Model_Room("Deluxe Room", "400 sqft", "1 Double Bed", "3733"));
        model_rooms.add(new Model_Room("Deluxe Suite Room", "450 sqft", "1 Double Bed", "4000"));
        model_rooms.add(new Model_Room("Superior Room", "269 sqft", "1 Double Bed", "3173"));
        model_rooms.add(new Model_Room("Superior Suite Room", "301 sqft", "1 Double Bed", "3553"));
        model_rooms.add(new Model_Room("Deluxe Room", "400 sqft", "1 Double Bed", "3733"));
        model_rooms.add(new Model_Room("Deluxe Suite Room", "450 sqft", "1 Double Bed", "4000"));
    }
}
