package com.example.customcomponents.listview;

public class Model_Room {
    private String room_type;
    private String room_area;
    private String room_size;
    private String room_price;

    public Model_Room(String room_type, String room_area, String room_size, String room_price) {
        this.room_type = room_type;
        this.room_area = room_area;
        this.room_size = room_size;
        this.room_price = room_price;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRoom_area() {
        return room_area;
    }

    public void setRoom_area(String room_area) {
        this.room_area = room_area;
    }

    public String getRoom_size() {
        return room_size;
    }

    public void setRoom_size(String room_size) {
        this.room_size = room_size;
    }

    public String getRoom_price() {
        return room_price;
    }

    @Override
    public String toString() {
        return "Model_Room{" +
                "room_type='" + room_type + '\'' +
                ", room_area='" + room_area + '\'' +
                ", room_size='" + room_size + '\'' +
                ", room_price='" + room_price + '\'' +
                '}';
    }

    public void setRoom_price(String room_price) {
        this.room_price = room_price;
    }
}
